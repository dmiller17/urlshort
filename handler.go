package urlshort

import (
	"net/http"

	"gopkg.in/yaml.v2"
)

type yamlMap []struct {
	Path string
	URL  string
}

// MapHandler will return an http.HandlerFunc (which also
// implements http.Handler) that will attempt to map any
// paths (keys in the map) to their corresponding URL (values
// that each key in the map points to, in string format).
// If the path is not provided in the map, then the fallback
// http.Handler will be called instead.
func MapHandler(pathsToUrls map[string]string, fallback http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		redirectPath, ok := pathsToUrls[r.URL.Path]
		if ok {
			http.Redirect(w, r, redirectPath, http.StatusFound)
		} else {
			fallback.ServeHTTP(w, r)
		}
	}
}

// YAMLHandler will parse the provided YAML and then return
// an http.HandlerFunc (which also implements http.Handler)
// that will attempt to map any paths to their corresponding
// URL. If the path is not provided in the YAML, then the
// fallback http.Handler will be called instead.
//
// YAML is expected to be in the format:
//
//     - path: /some-path
//       url: https://www.some-url.com/demo
//
// The only errors that can be returned all related to having
// invalid YAML data.
//
// See MapHandler to create a similar http.HandlerFunc via
// a mapping of paths to urls.
func YAMLHandler(yml []byte, fallback http.Handler) (http.HandlerFunc, error) {
	// todo: implement this...
	t := yamlMap{}
	err := yaml.Unmarshal(yml, &t)

	if err != nil {
		return fallback.ServeHTTP, err
	}
	nativeMap := yamlToMap(t)

	return MapHandler(nativeMap, fallback), nil
}

// DBHandler will load short URLs from a boltdb file
func DBHandler(db *BoltDBKeyValueStore, fallback http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		path := db.Get(r.URL.Path)

		if path != "" {
			http.Redirect(w, r, path, http.StatusFound)
		} else {
			fallback.ServeHTTP(w, r)
		}
	}
}

func yamlToMap(yml yamlMap) map[string]string {
	nativeMap := make(map[string]string)

	for _, entry := range yml {
		nativeMap[entry.Path] = entry.URL
	}

	return nativeMap
}
