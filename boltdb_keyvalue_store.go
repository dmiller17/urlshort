package urlshort

import "github.com/boltdb/bolt"

const bucket string = "urlshort"

// BoltDBKeyValueStore implements db_interface
type BoltDBKeyValueStore struct {
	db *bolt.DB
}

// NewBoltDBKeyValueStore returns an initialized kv store
func NewBoltDBKeyValueStore(db *bolt.DB) *BoltDBKeyValueStore {
	b := new(BoltDBKeyValueStore)
	b.db = db

	return b
}

// Get returns a value
func (b *BoltDBKeyValueStore) Get(key string) string {
	var value []byte
	b.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucket))
		value = b.Get([]byte(key))
		return nil
	})

	return string(value)
}
