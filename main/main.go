package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/boltdb/bolt"
	"github.com/gophercises/urlshort"
)

func main() {
	mux := defaultMux()

	db, err := bolt.Open("my.db", 0600, nil)

	if err != nil {
		panic(err)
	}

	defer db.Close()

	dbMap := map[string]string{
		"/goog": "https://google.com",
		"/s":    "https://www.something.com",
	}

	db.Update(func(tx *bolt.Tx) error {
		bucketName := []byte("urlshort")
		err := tx.DeleteBucket(bucketName)

		if err != nil {
			log.Printf("Error: couldn't delete bucket %v, got error %v", bucketName, err)
		}

		b, err := tx.CreateBucket(bucketName)

		if err != nil {
			panic(err)
		}

		for k, v := range dbMap {
			b.Put([]byte(k), []byte(v))
		}

		return nil
	})

	if err != nil {
		panic(err)
	}

	kvStore := urlshort.NewBoltDBKeyValueStore(db)

	dbHandler := urlshort.DBHandler(kvStore, mux)

	// Build the MapHandler using the mux as the fallback
	pathsToUrls := map[string]string{
		"/urlshort-godoc": "https://godoc.org/github.com/gophercises/urlshort",
		"/yaml-godoc":     "https://godoc.org/gopkg.in/yaml.v2",
	}
	mapHandler := urlshort.MapHandler(pathsToUrls, dbHandler)

	// Build the YAMLHandler using the mapHandler as the
	// fallback
	yaml := `
- path: /urlshort
  url: https://github.com/gophercises/urlshort
- path: /urlshort-final
  url: https://github.com/gophercises/urlshort/tree/solution
`
	yamlHandler, err := urlshort.YAMLHandler([]byte(yaml), mapHandler)
	if err != nil {
		panic(err)
	}
	fmt.Println("Starting the server on :8080")
	http.ListenAndServe(":8080", yamlHandler)
}

func defaultMux() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/", hello)
	return mux
}

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, world!")
}
