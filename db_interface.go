package urlshort

// KeyValueStore provides an interface around fetching keys
type KeyValueStore interface {
	Get(key string) string
}
